/*!
This crate provide two serializable packet : [`AlertPacket`] and [`AlivePacket`]. They are used
by the [`canary_collector`] and [`packet_canary`] crates.

[`canary_collector`]: ../../canary_collector/index.html
[`packet_canary`]: ../../packet_canary/index.html
*/
use bincode::Result;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct AlertPacket {
    pub source: String,
    pub destination: String,
    /// IPv4, Ipv6, ICMP,…
    pub ethertype: String,
    pub length: usize,
    /// Something like "\[wlp58s0\]: TCP Packet: 174.141.200.41:9002 > 192.168.1.108:34030; length: 575"
    pub description: String,
    pub source_ip: Option<String>,
    pub dest_ip: Option<String>,
    pub next_proto: Option<String>,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct AlivePacket {}

/// A [`CanaryPacket`] which can be serialized in order to be transmitted as an UDP payload.
#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub enum CanaryPacket {
    Alert(AlertPacket),
    Alive(AlivePacket),
}

pub trait Serializable: Sized {
    fn encode(&self) -> Result<Vec<u8>>;
    fn decode(bytes: &[u8]) -> Result<Self>;
}

/// A [`CanaryPacket`] is serialized via [`bincode`] (but it can be changed).
impl Serializable for CanaryPacket {
    fn encode(&self) -> Result<Vec<u8>> {
        bincode::serialize(self)
    }
    fn decode(bytes: &[u8]) -> Result<Self> {
        bincode::deserialize(&bytes)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn decode_encode_is_dempotent() {
        let cp = CanaryPacket::Alert {
            0: AlertPacket {
                source: "s".to_string(),
                destination: "d".to_string(),
                ethertype: "e".to_string(),
                length: 20,
                description: "Desc".to_string(),
                source_ip: None,
                dest_ip: None,
                next_proto: Some("Icmp".to_string()),
            },
        };
        assert_eq!(cp, CanaryPacket::decode(&cp.encode().unwrap()).unwrap());
    }
}
