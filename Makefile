
all: fmt doc clippy

fmt:
	cargo fmt

clippy:
	cargo clippy --workspace

doc: README.md
	 cargo +nightly doc --workspace --no-deps

.PHONY: target/debug/packet-canary
.PHONY: target/debug/canary-collector

packet-canary/src/main.rs: target/debug/packet-canary target/debug/canary-collector
	cargo build
	cp graph-rs.md graph.md
	mdsh -i packet-canary/src/main.rs --work_dir .

README.md: packet-canary/src/main.rs packet-canary/Cargo.toml
	(echo "<!-- ⚠⚠⚠ DO NOT EDIT, generated from packet-canary/src/main.rs and packet-canary/README.tpl ⚠⚠⚠ -->"; \
	 cargo readme -r packet-canary) > $@
	cp graph-gitlab.md graph.md
	mdsh  --work_dir .
