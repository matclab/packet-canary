<!-- ⚠⚠⚠ DO NOT EDIT, generated from packet-canary/src/main.rs and packet-canary/README.tpl ⚠⚠⚠ -->
[![Build Status](https://gitlab.com/matclab/packet-canary/badges/master/build.svg)](https://gitlab.com/matclab/packet-canary/commits/master)

# packet-canary

A network canary that listen to the network for unexpected packets (think about nmap scan used by intruder to map network assets) and an associated prometheus exporter.

[[_TOC_]]

## Architecture

The solution is composed of a packet detector (the *packet canary*) running on
a separate host (for example a RPi) whose sole purpose is to be the canary.
The canary shall never receive a packet. If it receives one it sends an alert as
an UDP packet to the preconfigured *canary collector*.

<!-- `> cat graph.md` -->
<!-- BEGIN mdsh -->
```mermaid
graph LR
  rpi1[packet canary 1]-- UDP -->cc[canary collector]<-- http -->prometheus
  rpi2[packet canary 2]-- UDP -->cc
```
<!-- END mdsh -->

The *packet canary* also regularly send an *alive* packet to show to the
*canary collector* it is up an running.

The *canary collector* provides a prometheus metric that may be
aggregated by a prometheus server and upon which some alerts may be
configured.

## Usage
### packet-canary
<!-- `$ target/debug/packet-canary -h` as text -->
```text
packet-canary 0.0.4
Listen on a given interface and send an alert to an UDP collector
if some packets are received.

Liveness packets are also sent regularly.

USAGE:
    packet-canary [FLAGS] [OPTIONS]

FLAGS:
    -b, --broadcast    report broadcast packets
    -h, --help         Prints help information
    -m, --multicast    report multicast packets
    -q, --quiet        Silence all output
    -V, --version      Prints version information
    -v, --verbose      Verbose mode (-v, -vv, -vvv, etc)

OPTIONS:
    -a, --address <address>            address of the UDP alert collector [default: 127.0.0.1]
    -i, --interface <interface>        interface on which we listen for packets [default: eth0]
    -M, --mac-address <mac-address>    mac address of the UDP alert collector. If given, ARP packet comming from this
                                       address are no reported
    -p, --port <port>                  port of the UDP alert collector [default: 17491]
    -s, --seconds <seconds>            Number of seconds between two alive packets [default: 60]
    -t, --timestamp <ts>               Timestamp (sec, ms, ns, none)
```

### canary-collector
<!-- `$ target/debug/canary-collector -h` as text -->
```text
canary-collector 0.0.4
Listen on a given interface for UDP alert packets
and send alerts

USAGE:
    canary-collector [FLAGS] [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -q, --quiet      Silence all output
    -V, --version    Prints version information
    -v, --verbose    Verbose mode (-v, -vv, -vvv, etc)

OPTIONS:
    -a, --address <address>                    address of the interface on which to listen to packet-canary [default:
                                               0.0.0.0]
    -p, --port <port>                          port of the UDP alert collector [default: 17491]
    -P, --prometheus-port <prometheus-port>    port for the prometheus exporter [default: 9700]
    -t, --timestamp <ts>                       Timestamp (sec, ms, ns, none)
```


### Prometheus Metrics
The `canary-collector` program provides two prometheus metric on the `prometheus-port`:
- `canary_packet_received` which are counters with labels `source`, `destination` and
  `ethertype`,
- `canary_alive_packet_received` which is a counter for liveness packet, which allows to raise
  some alerts if the `packet-canary` went down.

```sh
$ curl localhost:9702/metrics

# HELP canary_alive_packet_received Number of alive packet received
# TYPE canary_alive_packet_received counter
canary_alive_packet_received{source="192.168.1.67:13857"} 11
# HELP canary_packet_received Number of packet received
# TYPE canary_packet_received counter
canary_packet_received{destination="e7:a7:a3:a5:5e:d9",ethertype="Arp",source="f4:ca:e5:53:a9:27"} 1
canary_packet_received{destination="e7:a7:a3:a5:5e:d9",ethertype="Ipv4",source="02:fc:33:57:42:d6",source_ip="172.19.0.5",dest_ip="172.19.0.7",next_proto="Icmp"} 15
canary_packet_received{destination="e7:a7:a3:a5:5e:d9",ethertype="Ipv4",source="f4:ca:e5:53:a9:27",source_ip="172.19.0.4",dest_ip="172.19.0.7",next_proto="Icmp"} 32
canary_packet_received{destination="ff:ff:ff:ff:ff:ff",ethertype="Arp",source="80:1f:02:f2:fa:c0"} 11
```



## Compilation

For the host version you can build the `canary-collector` and `packet-canary`
with:
```
cargo build --release
```
The binaries are then found in the `target/release` directory.

For a cross compilation of `packet-canary` for Raspberry-Pi, you can do:
```
cargo install cross
cross build --target aarch64-unknown-linux-musl --release --workspace --exclude canary-collector
```
The binary is found in the `target/aarch64-unknown-linux-musl/release`
directory.

## Installation
You can either compile yourself or download the latest binaries from the
[release page](https://gitlab.com/matclab/packet-canary/-/releases).

Then use your favorite service manager (systemd, s6) to launch them.

## Prometheus Configuration
The prometheus configuration may contains something like:
```yaml
global:
  scrape_interval:     15s # By default, scrape targets every 15 seconds.
  evaluation_interval: 15s 

rule_files:
  - '*.rules'

alerting:
  alertmanagers:
  - scheme: http
    static_configs:
    - targets:
      - "alertmanager:9093"

scrape_configs:
  - job_name: 'packet-canary'
    static_configs:
      - targets: ['collector:9700']

```

And the `/etc/prometheus` directory may contain the `packet-canary.rules`:
```yaml
groups:
- name: canary
  rules:
  - alert: UnexpectedPacket
    expr: canary_packet_received > 0
    labels:
      severity: critical
    annotations:
      summary: "Unexpected {{ $labels.ethertype }} packet received ({{ $labels.source }} → {{ $labels.destination }})"

  - alert: CanaryIsDown
    expr: rate(canary_alive_packet_received[30s])*60 == 0
    labels:
      severity: high
    annotations:
      summary: "No more alive packet received from {{ $labels.source }}"

  - alert: CanaryLosePackets
    expr: rate(canary_alive_packet_received[1m])*60 < 12
    labels:
      severity: warning
    annotations:
      summary: "Less than 12 alive packets/mn ({{ $value }}) from {{ $labels.source }}"
```



# Trying with docker compose

The provided `docker-compose.yml` file allows to see the canary in action
after having build the two binaries with `cargo build`.

In a first terminal run: `docker-compose up`.
In  a second terminal run: `docker-compose exec rogue ping -c2 canary`.

The *canary collector* will log:
```log
collector_1  | INFO - Listening on 0.0.0.0:9700 for prometheus query
collector_1  | INFO - Listening on 0.0.0.0:17491 for canary data
collector_1  | INFO - > Ipv4:98 02:42:ac:12:00:03→02:42:ac:12:00:04 :: [eth0]: ICMP echo request 172.18.0.3 -> 172.18.0.4 (seq=0, id=2048)
collector_1  | INFO - > Ipv4:98 02:42:ac:12:00:03→02:42:ac:12:00:04 :: [eth0]: ICMP echo request 172.18.0.3 -> 172.18.0.4 (seq=1, id=2048)
collector_1  | INFO - > Arp:42 02:42:ac:12:00:03→02:42:ac:12:00:04 :: [eth0]: ARP packet: 02:42:ac:12:00:03(172.18.0.3) > 02:42:ac:12:00:04(172.18.0.4); operation: ArpOperation(2)
```

And then running `curl localhost:9700/metrics` will return something like:
```sh
# HELP canary_alive_packet_received Number of alive packet received
# TYPE canary_alive_packet_received counter
canary_alive_packet_received{source="172.18.0.4:43917"} 32
# HELP canary_packet_received Number of packet received
# TYPE canary_packet_received counter
canary_packet_received{destination="02:42:ac:12:00:04",ethertype="Arp",source="02:42:ac:12:00:03"} 1
canary_packet_received{destination="02:42:ac:12:00:04",ethertype="Ipv4",source="02:42:ac:12:00:03",source_ip="172.18.0.3",dest_ip="172.18.0.4",next_proto="Icmp"} 
2
```

You can see the corresponding metrics on the localhost prometheus web server (http://localhost:9090/graph?g0.range_input=30m&g0.expr=canary_packet_received&g0.tab=1) as well as the alert that has been triggered (http://localhost:9090/alerts).

Doing a `docker-compose stop canary` will also trigger a `CanaryIsDown` alert.

# License

Licensed under Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE) or https://www.apache.org/licenses/LICENSE-2.0)


## Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be licensed as above, without any additional terms or
conditions.
