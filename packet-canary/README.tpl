{{badges}}

# {{crate}}

{{readme}}

## Compilation

For the host version you can build the `canary-collector` and `packet-canary`
with:
```
cargo build --release
```
The binaries are then found in the `target/release` directory.

For a cross compilation of `packet-canary` for Raspberry-Pi, you can do:
```
cargo install cross
cross build --target aarch64-unknown-linux-musl --release --workspace --exclude canary-collector
```
The binary is found in the `target/aarch64-unknown-linux-musl/release`
directory.

## Installation
You can either compile yourself or download the latest binaries from the
[release page](https://gitlab.com/matclab/packet-canary/-/releases).

Then use your favorite service manager (systemd, s6) to launch them.

## Prometheus Configuration
The prometheus configuration may contains something like:
```yaml
global:
  scrape_interval:     15s # By default, scrape targets every 15 seconds.
  evaluation_interval: 15s 

rule_files:
  - '*.rules'

alerting:
  alertmanagers:
  - scheme: http
    static_configs:
    - targets:
      - "alertmanager:9093"

scrape_configs:
  - job_name: 'packet-canary'
    static_configs:
      - targets: ['collector:9700']

```

And the `/etc/prometheus` directory may contain the `packet-canary.rules`:
```yaml
groups:
- name: canary
  rules:
  - alert: UnexpectedPacket
    expr: canary_packet_received > 0
    labels:
      severity: critical
    annotations:
      summary: "Unexpected {{ $labels.ethertype }} packet received ({{ $labels.source }} → {{ $labels.destination }})"

  - alert: CanaryIsDown
    expr: rate(canary_alive_packet_received[30s])*60 == 0
    labels:
      severity: high
    annotations:
      summary: "No more alive packet received from {{ $labels.source }}"

  - alert: CanaryLosePackets
    expr: rate(canary_alive_packet_received[1m])*60 < 12
    labels:
      severity: warning
    annotations:
      summary: "Less than 12 alive packets/mn ({{ $value }}) from {{ $labels.source }}"
```



# Trying with docker compose

The provided `docker-compose.yml` file allows to see the canary in action
after having build the two binaries with `cargo build`.

In a first terminal run: `docker-compose up`.
In  a second terminal run: `docker-compose exec rogue ping -c2 canary`.

The *canary collector* will log:
```log
collector_1  | INFO - Listening on 0.0.0.0:9700 for prometheus query
collector_1  | INFO - Listening on 0.0.0.0:17491 for canary data
collector_1  | INFO - > Ipv4:98 02:42:ac:12:00:03→02:42:ac:12:00:04 :: [eth0]: ICMP echo request 172.18.0.3 -> 172.18.0.4 (seq=0, id=2048)
collector_1  | INFO - > Ipv4:98 02:42:ac:12:00:03→02:42:ac:12:00:04 :: [eth0]: ICMP echo request 172.18.0.3 -> 172.18.0.4 (seq=1, id=2048)
collector_1  | INFO - > Arp:42 02:42:ac:12:00:03→02:42:ac:12:00:04 :: [eth0]: ARP packet: 02:42:ac:12:00:03(172.18.0.3) > 02:42:ac:12:00:04(172.18.0.4); operation: ArpOperation(2)
```

And then running `curl localhost:9700/metrics` will return something like:
```sh
# HELP canary_alive_packet_received Number of alive packet received
# TYPE canary_alive_packet_received counter
canary_alive_packet_received{source="172.18.0.4:43917"} 32
# HELP canary_packet_received Number of packet received
# TYPE canary_packet_received counter
canary_packet_received{destination="02:42:ac:12:00:04",ethertype="Arp",source="02:42:ac:12:00:03"} 1
canary_packet_received{destination="02:42:ac:12:00:04",ethertype="Ipv4",source="02:42:ac:12:00:03",source_ip="172.18.0.3",dest_ip="172.18.0.4",next_proto="Icmp"} 
2
```

You can see the corresponding metrics on the localhost prometheus web server (http://localhost:9090/graph?g0.range_input=30m&g0.expr=canary_packet_received&g0.tab=1) as well as the alert that has been triggered (http://localhost:9090/alerts).

Doing a `docker-compose stop canary` will also trigger a `CanaryIsDown` alert.

# License

Licensed under Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE) or https://www.apache.org/licenses/LICENSE-2.0)


## Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be licensed as above, without any additional terms or
conditions.
