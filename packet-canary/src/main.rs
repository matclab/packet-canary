// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

/*!
A network canary that listen to the network for unexpected packets (think about nmap scan used by intruder to map network assets) and an associated prometheus exporter.

[[_TOC_]]

# Architecture

The solution is composed of a packet detector (the *packet canary*) running on
a separate host (for example a RPi) whose sole purpose is to be the canary.
The canary shall never receive a packet. If it receives one it sends an alert as
an UDP packet to the preconfigured *canary collector*.

<!-- `> cat graph.md` -->
<!-- BEGIN mdsh -->
```text
   RPI1
packet canary  ---- UDP -----> canary collector  <----- HTTP -----> prometheus
                                     ^
   RPI2                              |
packet canary  ---- UDP -------------'
```
<!-- END mdsh -->

The *packet canary* also regularly send an *alive* packet to show to the
*canary collector* it is up an running.

The *canary collector* provides a prometheus metric that may be
aggregated by a prometheus server and upon which some alerts may be
configured.

# Usage
## packet-canary
<!-- `$ target/debug/packet-canary -h` as text -->
```text
packet-canary 0.0.4
Listen on a given interface and send an alert to an UDP collector
if some packets are received.

Liveness packets are also sent regularly.

USAGE:
    packet-canary [FLAGS] [OPTIONS]

FLAGS:
    -b, --broadcast    report broadcast packets
    -h, --help         Prints help information
    -m, --multicast    report multicast packets
    -q, --quiet        Silence all output
    -V, --version      Prints version information
    -v, --verbose      Verbose mode (-v, -vv, -vvv, etc)

OPTIONS:
    -a, --address <address>            address of the UDP alert collector [default: 127.0.0.1]
    -i, --interface <interface>        interface on which we listen for packets [default: eth0]
    -M, --mac-address <mac-address>    mac address of the UDP alert collector. If given, ARP packet comming from this
                                       address are no reported
    -p, --port <port>                  port of the UDP alert collector [default: 17491]
    -s, --seconds <seconds>            Number of seconds between two alive packets [default: 60]
    -t, --timestamp <ts>               Timestamp (sec, ms, ns, none)
```

## canary-collector
<!-- `$ target/debug/canary-collector -h` as text -->
```text
canary-collector 0.0.4
Listen on a given interface for UDP alert packets
and send alerts

USAGE:
    canary-collector [FLAGS] [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -q, --quiet      Silence all output
    -V, --version    Prints version information
    -v, --verbose    Verbose mode (-v, -vv, -vvv, etc)

OPTIONS:
    -a, --address <address>                    address of the interface on which to listen to packet-canary [default:
                                               0.0.0.0]
    -p, --port <port>                          port of the UDP alert collector [default: 17491]
    -P, --prometheus-port <prometheus-port>    port for the prometheus exporter [default: 9700]
    -t, --timestamp <ts>                       Timestamp (sec, ms, ns, none)
```


## Prometheus Metrics
The `canary-collector` program provides two prometheus metric on the `prometheus-port`:
- `canary_packet_received` which are counters with labels `source`, `destination` and
  `ethertype`,
- `canary_alive_packet_received` which is a counter for liveness packet, which allows to raise
  some alerts if the `packet-canary` went down.

```sh
$ curl localhost:9702/metrics

# HELP canary_alive_packet_received Number of alive packet received
# TYPE canary_alive_packet_received counter
canary_alive_packet_received{source="192.168.1.67:13857"} 11
# HELP canary_packet_received Number of packet received
# TYPE canary_packet_received counter
canary_packet_received{destination="e7:a7:a3:a5:5e:d9",ethertype="Arp",source="f4:ca:e5:53:a9:27"} 1
canary_packet_received{destination="e7:a7:a3:a5:5e:d9",ethertype="Ipv4",source="02:fc:33:57:42:d6",source_ip="172.19.0.5",dest_ip="172.19.0.7",next_proto="Icmp"} 15
canary_packet_received{destination="e7:a7:a3:a5:5e:d9",ethertype="Ipv4",source="f4:ca:e5:53:a9:27",source_ip="172.19.0.4",dest_ip="172.19.0.7",next_proto="Icmp"} 32
canary_packet_received{destination="ff:ff:ff:ff:ff:ff",ethertype="Arp",source="80:1f:02:f2:fa:c0"} 11
```


*/
use anyhow::Result;
use futures::TryFutureExt;
use log::info;
use structopt::StructOpt;
use tokio::task;

mod send;
use send::{send_alive, OwnUdpSocket};

mod snoop;
use snoop::collect_packets;

/// Struct defining command line options
#[derive(StructOpt, Debug, Clone)]
#[structopt(
    name = "packet-canary",
    about = "Listen on a given interface and send an alert to an UDP collector
if some packets are received.

Liveness packets are also sent regularly."
)]
pub struct Args {
    /// interface on which we listen for packets
    #[structopt(short, long, default_value = "eth0")]
    interface: String,

    /// address of the UDP alert collector
    #[structopt(short, long, default_value = "127.0.0.1")]
    address: String,

    /// mac address of the UDP alert collector. If given, ARP packet comming from this address are
    /// no reported
    #[structopt(short = "M", long)]
    mac_address: Option<String>,

    /// port of the UDP alert collector
    #[structopt(short, long, default_value = "17491")]
    port: u16,

    /// report broadcast packets
    #[structopt(name = "broadcast", short, long)]
    report_broadcast: bool,

    /// report multicast packets
    #[structopt(name = "multicast", short, long)]
    report_multicast: bool,

    // The number of occurrences of the `v/verbose` flag
    // Verbose mode (error, warn, info, debug)
    //#[structopt(short, long, default_value= "info")]
    //verbose: String,
    /// Number of seconds between two alive packets
    #[structopt(short, long, default_value = "60")]
    seconds: u8,

    /// Timestamp (sec, ms, ns, none)
    #[structopt(short = "t", long = "timestamp")]
    ts: Option<stderrlog::Timestamp>,

    /// Verbose mode (-v, -vv, -vvv, etc)
    #[structopt(short = "v", long = "verbose", parse(from_occurrences))]
    verbose: usize,

    /// Silence all output
    #[structopt(short = "q", long = "quiet")]
    quiet: bool,
}

fn info_about_args(args: &Args) {
    if !args.report_broadcast {
        info!("Broadcast packets won't be reported");
    }
    if !args.report_multicast {
        info!("Multicast packets won't be reported");
    }
    if let Some(mac) = &args.mac_address {
        info!("ARP packet from {} won't be reported", &mac.to_string());
    }
}

#[paw::main]
#[tokio::main]
async fn main(args: Args) -> Result<()> {
    stderrlog::new()
        .module(module_path!())
        .quiet(args.quiet)
        .verbosity(args.verbose)
        .timestamp(args.ts.unwrap_or(stderrlog::Timestamp::Off))
        .init()
        .unwrap();

    info_about_args(&args);
    let socket = OwnUdpSocket::bind("0.0.0.0:0").await?;
    let destaddr = format!("{}:{}", args.address, args.port);

    let send = send_alive(socket.clone(), destaddr.clone(), args.seconds);
    let collect = task::spawn(collect_packets(socket, destaddr, args));

    let res = tokio::try_join!(collect.map_err(anyhow::Error::new), send);
    match res {
        Ok(_) => Ok(()),
        Err(e) => Err(e),
    }
}
