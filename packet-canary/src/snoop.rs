//! The `snoop` module encapsulate the datalink packet snooping and decoding.
//! Note that the [`collect_packets`] is async, but only by mean of timeout and
//! regular yields.
use anyhow::{anyhow, Result};
use log::{debug, info};
use pnet::datalink::{self, NetworkInterface};
use pnet::packet::arp::ArpPacket;
use pnet::packet::ethernet::{EtherTypes, EthernetPacket, MutableEthernetPacket};
use pnet::packet::icmp::{echo_reply, echo_request, IcmpPacket, IcmpTypes};
use pnet::packet::icmpv6::Icmpv6Packet;
use pnet::packet::ip::{IpNextHeaderProtocol, IpNextHeaderProtocols};
use pnet::packet::ipv4::Ipv4Packet;
use pnet::packet::ipv6::Ipv6Packet;
use pnet::packet::tcp::TcpPacket;
use pnet::packet::udp::UdpPacket;
use pnet::packet::Packet;
use pnet::util::MacAddr;
use std::net::IpAddr;
use std::str::FromStr;
use tokio::task;
use tokio::time::Duration;

use packet_encoding::{AlertPacket, CanaryPacket};

use crate::send::{send_to_collector, OwnUdpSocket};
use crate::Args;

fn handle_udp_packet(
    interface_name: &str,
    source: IpAddr,
    destination: IpAddr,
    packet: &[u8],
) -> String {
    let udp = UdpPacket::new(packet);

    if let Some(udp) = udp {
        format!(
            "[{}]: UDP Packet: {}:{} > {}:{}; length: {}",
            interface_name,
            source,
            udp.get_source(),
            destination,
            udp.get_destination(),
            udp.get_length()
        )
    } else {
        format!("[{}]: Malformed UDP Packet", interface_name)
    }
}

fn handle_icmp_packet(
    interface_name: &str,
    source: IpAddr,
    destination: IpAddr,
    packet: &[u8],
) -> String {
    let icmp_packet = IcmpPacket::new(packet);
    if let Some(icmp_packet) = icmp_packet {
        match icmp_packet.get_icmp_type() {
            IcmpTypes::EchoReply => {
                let echo_reply_packet = echo_reply::EchoReplyPacket::new(packet).unwrap();
                format!(
                    "[{}]: ICMP echo reply {} -> {} (seq={:?}, id={:?})",
                    interface_name,
                    source,
                    destination,
                    echo_reply_packet.get_sequence_number(),
                    echo_reply_packet.get_identifier()
                )
            }
            IcmpTypes::EchoRequest => {
                let echo_request_packet = echo_request::EchoRequestPacket::new(packet).unwrap();
                format!(
                    "[{}]: ICMP echo request {} -> {} (seq={:?}, id={:?})",
                    interface_name,
                    source,
                    destination,
                    echo_request_packet.get_sequence_number(),
                    echo_request_packet.get_identifier()
                )
            }
            _ => format!(
                "[{}]: ICMP packet {} -> {} (type={:?})",
                interface_name,
                source,
                destination,
                icmp_packet.get_icmp_type()
            ),
        }
    } else {
        format!("[{}]: Malformed ICMP Packet", interface_name)
    }
}

fn handle_icmpv6_packet(
    interface_name: &str,
    source: IpAddr,
    destination: IpAddr,
    packet: &[u8],
) -> String {
    let icmpv6_packet = Icmpv6Packet::new(packet);
    if let Some(icmpv6_packet) = icmpv6_packet {
        format!(
            "[{}]: ICMPv6 packet {} -> {} (type={:?})",
            interface_name,
            source,
            destination,
            icmpv6_packet.get_icmpv6_type()
        )
    } else {
        format!("[{}]: Malformed ICMPv6 Packet", interface_name)
    }
}

fn handle_tcp_packet(
    interface_name: &str,
    source: IpAddr,
    destination: IpAddr,
    packet: &[u8],
) -> String {
    let tcp = TcpPacket::new(packet);
    if let Some(tcp) = tcp {
        format!(
            "[{}]: TCP Packet: {}:{} > {}:{}; length: {}",
            interface_name,
            source,
            tcp.get_source(),
            destination,
            tcp.get_destination(),
            packet.len()
        )
    } else {
        format!("[{}]: Malformed TCP Packet", interface_name)
    }
}

fn handle_transport_protocol(
    interface_name: &str,
    source: IpAddr,
    destination: IpAddr,
    protocol: IpNextHeaderProtocol,
    packet: &[u8],
) -> String {
    match protocol {
        IpNextHeaderProtocols::Udp => {
            handle_udp_packet(interface_name, source, destination, packet)
        }
        IpNextHeaderProtocols::Tcp => {
            handle_tcp_packet(interface_name, source, destination, packet)
        }
        IpNextHeaderProtocols::Icmp => {
            handle_icmp_packet(interface_name, source, destination, packet)
        }
        IpNextHeaderProtocols::Icmpv6 => {
            handle_icmpv6_packet(interface_name, source, destination, packet)
        }
        _ => format!(
            "[{}]: Unknown {} packet: {} > {}; protocol: {:?} length: {}",
            interface_name,
            match source {
                IpAddr::V4(..) => "IPv4",
                _ => "IPv6",
            },
            source,
            destination,
            protocol,
            packet.len()
        ),
    }
}

fn handle_ipv4_packet(interface_name: &str, ethernet: &EthernetPacket) -> Result<CanaryPacket> {
    let header = Ipv4Packet::new(ethernet.payload());
    let message;
    if let Some(header) = header {
        message = handle_transport_protocol(
            interface_name,
            IpAddr::V4(header.get_source()),
            IpAddr::V4(header.get_destination()),
            header.get_next_level_protocol(),
            header.payload(),
        );
        Ok(CanaryPacket::Alert {
            0: AlertPacket {
                source: ethernet.get_source().to_string(),
                destination: ethernet.get_destination().to_string(),
                ethertype: ethernet.get_ethertype().to_string(),
                length: ethernet.packet().len(),
                description: message,
                source_ip: Some(header.get_source().to_string()),
                dest_ip: Some(header.get_destination().to_string()),
                next_proto: Some(header.get_next_level_protocol().to_string()),
            },
        })
    } else {
        Err(anyhow!("[{}]: Malformed IPv4 Packet", interface_name))
    }
}

fn handle_ipv6_packet(interface_name: &str, ethernet: &EthernetPacket) -> Result<CanaryPacket> {
    let header = Ipv6Packet::new(ethernet.payload());
    let message;
    if let Some(header) = header {
        message = handle_transport_protocol(
            interface_name,
            IpAddr::V6(header.get_source()),
            IpAddr::V6(header.get_destination()),
            header.get_next_header(),
            header.payload(),
        );
        Ok(CanaryPacket::Alert {
            0: AlertPacket {
                source: ethernet.get_source().to_string(),
                destination: ethernet.get_destination().to_string(),
                ethertype: ethernet.get_ethertype().to_string(),
                length: ethernet.packet().len(),
                description: message,
                source_ip: Some(header.get_source().to_string()),
                dest_ip: Some(header.get_destination().to_string()),
                next_proto: Some(header.get_next_header().to_string()),
            },
        })
    } else {
        Err(anyhow!("[{}]: Malformed IPv6 Packet", interface_name))
    }
}

fn handle_arp_packet(interface_name: &str, ethernet: &EthernetPacket) -> Result<CanaryPacket> {
    let header = ArpPacket::new(ethernet.payload());
    let message;
    if let Some(header) = header {
        message = format!(
            "[{}]: ARP packet: {}({}) > {}({}); operation: {:?}",
            interface_name,
            ethernet.get_source(),
            header.get_sender_proto_addr(),
            ethernet.get_destination(),
            header.get_target_proto_addr(),
            header.get_operation()
        );
        Ok(CanaryPacket::Alert {
            0: AlertPacket {
                source: ethernet.get_source().to_string(),
                destination: ethernet.get_destination().to_string(),
                ethertype: ethernet.get_ethertype().to_string(),
                length: ethernet.packet().len(),
                description: message,
                source_ip: None,
                dest_ip: None,
                next_proto: None,
            },
        })
    } else {
        Err(anyhow!("[{}]: Malformed ARP Packet", interface_name))
    }
}

fn handle_ethernet_frame(interface: &NetworkInterface, ethernet: &EthernetPacket) -> CanaryPacket {
    let interface_name = &interface.name[..];

    let packet = match ethernet.get_ethertype() {
        EtherTypes::Ipv4 => handle_ipv4_packet(interface_name, ethernet),
        EtherTypes::Ipv6 => handle_ipv6_packet(interface_name, ethernet),
        EtherTypes::Arp => handle_arp_packet(interface_name, ethernet),
        _ => {
            let message = format!(
                "[{}]: Unknown packet: {} > {}; ethertype: {:?} length: {}",
                interface_name,
                ethernet.get_source(),
                ethernet.get_destination(),
                ethernet.get_ethertype(),
                ethernet.packet().len()
            );
            Ok(CanaryPacket::Alert {
                0: AlertPacket {
                    source: ethernet.get_source().to_string(),
                    destination: ethernet.get_destination().to_string(),
                    ethertype: ethernet.get_ethertype().to_string(),
                    length: ethernet.packet().len(),
                    description: message,
                    source_ip: None,
                    dest_ip: None,
                    next_proto: None,
                },
            })
        }
    };
    match packet {
        Ok(p) => p,
        Err(s) => CanaryPacket::Alert {
            0: AlertPacket {
                source: ethernet.get_source().to_string(),
                destination: ethernet.get_destination().to_string(),
                ethertype: ethernet.get_ethertype().to_string(),
                length: ethernet.packet().len(),
                description: s.to_string(),
                source_ip: None,
                dest_ip: None,
                next_proto: None,
            },
        },
    }
}

/// Collect packet, discard those coming from own mac address and send them to the
/// [`canary_collector`] node.
///
/// Note that this function is async, but only by mean of timeout and regular yields (should be
/// rewrite once [`pnet`] provide some async interface).
///
/// [`canary_collector`]: ../../canary_collector/index.html
/// [`pnet`]: https://docs.rs/pnet/0.25.0/pnet/
pub async fn collect_packets(socket: OwnUdpSocket, destaddr: String, args: Args) -> Result<()> {
    use pnet::datalink::Channel::Ethernet;
    let interface_names_match = |iface: &NetworkInterface| iface.name == args.interface;

    // Find the network interface with the provided name
    let interfaces = datalink::interfaces();
    let interface = interfaces
        .into_iter()
        .find(interface_names_match)
        .unwrap_or_else(|| panic!("Unable to find interface {}", args.interface));
    let mac = interface
        .mac
        .expect("No mac addres for specified interface");

    info!(
        "Listening for unexpected packets on interface {} ({})",
        interface, mac
    );

    // Create a channel to receive on
    let conf = datalink::Config {
        read_timeout: Some(Duration::new(0, 100)),
        promiscuous: false,
        ..Default::default()
    };
    let (_, mut rx) = match datalink::channel(&interface, conf) {
        Ok(Ethernet(tx, rx)) => (tx, rx),
        Ok(_) => panic!("packetdump: unhandled channel type"),
        Err(e) => panic!("packetdump: unable to create channel: {}", e),
    };

    loop {
        let mut buf: [u8; 1600] = [0u8; 1600];
        let mut fake_ethernet_frame = MutableEthernetPacket::new(&mut buf[..]).unwrap();
        match rx.next() {
            Ok(packet) => {
                let payload_offset;
                if cfg!(target_os = "macos")
                    && interface.is_up()
                    && !interface.is_broadcast()
                    && ((!interface.is_loopback() && interface.is_point_to_point())
                        || interface.is_loopback())
                {
                    if interface.is_loopback() {
                        // The pnet code for BPF loopback adds a zero'd out Ethernet header
                        payload_offset = 14;
                    } else {
                        // Maybe is TUN interface
                        payload_offset = 0;
                    }
                    if packet.len() > payload_offset {
                        let version = Ipv4Packet::new(&packet[payload_offset..])
                            .unwrap()
                            .get_version();
                        if version == 4 {
                            fake_ethernet_frame.set_destination(MacAddr(0, 0, 0, 0, 0, 0));
                            fake_ethernet_frame.set_source(MacAddr(0, 0, 0, 0, 0, 0));
                            fake_ethernet_frame.set_ethertype(EtherTypes::Ipv4);
                            fake_ethernet_frame.set_payload(&packet[payload_offset..]);
                            let res = handle_ethernet_frame(
                                &interface,
                                &fake_ethernet_frame.to_immutable(),
                            );
                            send_to_collector(&socket, &res, &destaddr).await?;
                            continue;
                        } else if version == 6 {
                            fake_ethernet_frame.set_destination(MacAddr(0, 0, 0, 0, 0, 0));
                            fake_ethernet_frame.set_source(MacAddr(0, 0, 0, 0, 0, 0));
                            fake_ethernet_frame.set_ethertype(EtherTypes::Ipv6);
                            fake_ethernet_frame.set_payload(&packet[payload_offset..]);
                            let res = handle_ethernet_frame(
                                &interface,
                                &fake_ethernet_frame.to_immutable(),
                            );
                            send_to_collector(&socket, &res, &destaddr).await?;
                            continue;
                        }
                    }
                }
                let ethpacket = EthernetPacket::new(packet).unwrap();
                // Skip packet send by ourself
                if ethpacket.get_source() == mac {
                    debug!("Ignore packet from self");
                    continue;
                }
                if let Some(collector_mac) = &args.mac_address {
                    if MacAddr::from_str(collector_mac).unwrap() == ethpacket.get_source()
                        && ethpacket.get_ethertype() == EtherTypes::Arp
                    {
                        debug!("Ignore ARP packet from {}", collector_mac);
                        continue;
                    }
                }
                // Skip broadcast packets except if asked on CLI
                if !args.report_broadcast && ethpacket.get_destination().is_broadcast() {
                    debug!("Ignore broadcast packet");
                    continue;
                }
                // Skip multicast packets except if asked on CLI
                if !args.report_multicast && ethpacket.get_destination().is_multicast() {
                    debug!("Ignore multicast packet");
                    continue;
                }
                // Skip packet not being send to us
                if ethpacket.get_destination() != mac
                    && !(ethpacket.get_destination().is_multicast()
                        || ethpacket.get_destination().is_broadcast())
                {
                    continue;
                }
                let res = handle_ethernet_frame(&interface, &ethpacket);
                send_to_collector(&socket, &res, &destaddr).await?;
            }
            Err(e) => match e.kind() {
                std::io::ErrorKind::TimedOut => {}
                _ => panic!("packetdump: unable to receive packet: {:?}", e),
            },
        }
        task::yield_now().await;
    }
}
