//! This module encapsulates the function to send UDP packets to the [`canary_collector`] node.
//!
//! [`canary_collector`]: ../../canary_collector/index.html
use anyhow::Result;
use futures::lock::Mutex;
use log::{debug, info};
use packet_encoding::{AlivePacket, CanaryPacket, Serializable};
use std::sync::Arc;
use tokio::net::{ToSocketAddrs, UdpSocket};
use tokio::time::{self, Duration};

/// The [`OwnUdpSocket`] is a clonable wrapper to [`UdpSocket`] in order to allow simultaneous access
/// to the same socket by several tasks.
#[derive(Clone)]
pub struct OwnUdpSocket {
    socket: Arc<Mutex<UdpSocket>>,
}

impl OwnUdpSocket {
    pub async fn send_to<A: ToSocketAddrs>(&self, buf: &[u8], target: A) -> Result<usize> {
        match self.socket.lock().await.send_to(buf, target).await {
            Ok(r) => Ok(r),
            Err(e) => Err(e.into()),
        }
    }

    pub async fn bind<A: ToSocketAddrs>(addr: A) -> Result<Self> {
        Ok(OwnUdpSocket {
            socket: Arc::new(Mutex::new(UdpSocket::bind(addr).await?)),
        })
    }
}
/// Send a UDP [`CanaryPacket`] to a [`canary_collector`]
/// - `socket`: on which to send the packet
/// - `addr`: the destination address
///
/// [`canary_collector`]: ../../canary_collector/index.html
pub async fn send_to_collector(
    socket: &OwnUdpSocket,
    packet: &CanaryPacket,
    addr: &str,
) -> Result<()> {
    match packet {
        CanaryPacket::Alert(p) => debug!("< {}", p.description),
        CanaryPacket::Alive(_) => debug!("< [Alive Packet]"),
    };
    debug!("> sending to {}", addr);
    match packet.encode() {
        Ok(ref mut s) => {
            socket.send_to(&s, addr).await?;
            Ok(())
        }
        Err(e) => Err(e.into()),
    }
}

/// Send an UDP [`AlivePacket`]s on `socket` to `destaddr` each `seconds`.
pub async fn send_alive(socket: OwnUdpSocket, destaddr: String, seconds: u8) -> Result<()> {
    let mut interval = time::interval(Duration::new(seconds.into(), 0));
    let packet = CanaryPacket::Alive { 0: AlivePacket {} };
    info!("Sending packets to {}", destaddr);
    loop {
        interval.tick().await;
        send_to_collector(&socket, &packet, &destaddr).await?
    }
}
