/*!
This module is responsible for collecting packets from one or several [`packet_canary`] instances and building appropriate metrics as described in [`packet_canary`] main documentation.

[`packet_canary`]: ../../packet_canary/index.html
*/
use anyhow::Result;
use log::{debug, error, info};
use packet_encoding::{CanaryPacket, Serializable};
use std::net::Ipv4Addr;
use structopt::StructOpt;
use tokio::net::UdpSocket;
extern crate prometheus;
use lazy_static::lazy_static;
use prometheus::{labels, register_int_counter_vec, Encoder, IntCounterVec, TextEncoder};
use warp::Filter;

/// Struct defining command line options
#[derive(StructOpt, Debug)]
#[structopt(
    name = "canary-collector",
    about = "Listen on a given interface for UDP alert packets
and send alerts"
)]
struct Args {
    /// address of the interface on which to listen to packet-canary
    #[structopt(short, long, default_value = "0.0.0.0")]
    address: String,

    /// port of the UDP alert collector
    #[structopt(short, long, default_value = "17491")]
    port: u16,

    /// port for the prometheus exporter
    #[structopt(short = "P", long, default_value = "9700")]
    prometheus_port: u16,

    /// Timestamp (sec, ms, ns, none)
    #[structopt(short = "t", long = "timestamp")]
    ts: Option<stderrlog::Timestamp>,

    /// Verbose mode (-v, -vv, -vvv, etc)
    #[structopt(short = "v", long = "verbose", parse(from_occurrences))]
    verbose: usize,

    /// Silence all output
    #[structopt(short = "q", long = "quiet")]
    quiet: bool,
}

/// Received packet size limit
const MAX_PAYLOAD_LENGTH: usize = 4096;

lazy_static! {
    ///  Prometheus counters for suspicious packets received
    static ref PACKET_COUNTER: IntCounterVec = register_int_counter_vec!(
        "canary_packet_received",
        "Number of packet received",
        &["source", "destination", "ethertype", "source_ip", "dest_ip", "next_proto"]
    )
    .unwrap();
    ///  Prometheus counters for alive packets received
    static ref ALIVE_COUNTER: IntCounterVec = register_int_counter_vec!(
        "canary_alive_packet_received",
        "Number of alive packet received",
        &["source"]
    )
    .unwrap();
}

/// Warp web server for serving prometheus metrics on `/metrics` url.
async fn prometheus_metric_server(port: u16) -> Result<()> {
    let routes = warp::path!("metrics").map(
        || {
            let encoder = TextEncoder::new();
            let metric_families = prometheus::gather();
            let mut buffer = vec![];
            encoder.encode(&metric_families, &mut buffer).unwrap();
            buffer
        }, //     )
    );
    info!("Listening on 0.0.0.0:{} for prometheus query ", port);
    warp::serve(routes).run(([0, 0, 0, 0], port)).await;
    Ok(())
}

/// Listen for [`packet_canary`] UDP packets (either [`AlertPacket`][packet_encoding::AlertPacket]s or [`AlivePacket`][packet_encoding::AlivePacket]s) and
/// increment the prometheus counter accordingly.
///
///[`packet_canary`]: ../../packet_canary/index.html
async fn udp_listener(address: &str, port: u16) -> Result<()> {
    info!("Listening on {}:{} for canary data", address, port);
    let addr = (address.parse::<Ipv4Addr>().unwrap(), port);
    let socket = UdpSocket::bind(&addr)
        .await
        .expect("Unable to bind UDP socket");

    loop {
        let mut buf = [0; MAX_PAYLOAD_LENGTH];
        let (_number_of_bytes, src_addr) = socket.recv_from(&mut buf).await?;
        debug!("< received from {}", src_addr);
        let packet = CanaryPacket::decode(&buf);
        match packet {
            Ok(p) => match p {
                CanaryPacket::Alert(p) => {
                    let ipinfo = match p.source_ip.clone() {
                        Some(sip) => format!(
                            "{}→{} {}",
                            sip,
                            p.dest_ip.clone().unwrap_or_default(),
                            p.next_proto.clone().unwrap_or_default()
                        ),
                        None => "".to_string(),
                    };
                    info!(
                        "> {}:{} {}→{} {} :: {}",
                        p.ethertype, p.length, p.source, p.destination, ipinfo, p.description
                    );
                    let sip = p.source_ip.unwrap_or_default();
                    let dip = p.dest_ip.unwrap_or_default();
                    let np = p.next_proto.unwrap_or_default();
                    let labels = labels! {"source"=> &*p.source, "destination"=> &*p.destination,
                            "ethertype"=> &*p.ethertype, "source_ip" => &*sip,
                            "dest_ip" => &*dip,
                            "next_proto" => &*np,
                    };

                    PACKET_COUNTER.get_metric_with(&labels)?.inc();
                }
                CanaryPacket::Alive(_) => {
                    let addr = src_addr.to_string();
                    ALIVE_COUNTER
                        .get_metric_with(&labels! {"source"=> addr.as_str(),})?
                        .inc();
                    debug!("> {} :: [Liveness Packet]", &addr);
                }
            },
            Err(p) => error!("{}", p),
        }
    }
}

#[paw::main]
#[tokio::main]
async fn main(args: Args) -> Result<()> {
    stderrlog::new()
        .module(module_path!())
        .quiet(args.quiet)
        .verbosity(args.verbose)
        .timestamp(args.ts.unwrap_or(stderrlog::Timestamp::Off))
        .init()
        .unwrap();

    match tokio::try_join!(
        prometheus_metric_server(args.prometheus_port),
        udp_listener(&args.address, args.port)
    ) {
        Ok(_) => Ok(()),
        Err(e) => Err(e),
    }
}
