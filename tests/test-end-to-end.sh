#!/bin/bash
set -e
set -o nounset
set -o pipefail

fail() {
   echo "FAIL"
   echo "### metrics and logs"
   docker-compose exec -T  collector sh -c 'curl -s localhost:9700/metrics' || true
   docker-compose logs || true
   docker-compose stop
   echo "FAIL"
}

docker-compose up -d
trap fail ERR SIGINT

sleep 5  # TODO: Use healthcheck in docker-compose to be able to test if service is up?

echo
echo "### Logs"
docker-compose logs

echo
echo "### Check we received some alive packets"
docker-compose exec -T  collector \
   sh -c "curl -s localhost:9700/metrics | grep ^canary_alive_packet_received "


echo
echo "### Send ping from rogue machine"
docker-compose exec -T rogue ip neigh flush all # clear ARP cache
docker-compose exec -T rogue ping -c2 canary
echo
sleep 1
echo "### Check we signal the rogues packets"
nb=$(docker-compose exec -T  collector \
   sh -c 'curl -s localhost:9700/metrics | grep "^canary_packet_received.*Ipv4.*$"' |
   cut -f2 -d' ')

test "$nb" -ge 2

echo
echo "### Send ping from rogue machine"
docker-compose exec -T rogue ping -c2 canary

sleep 1
echo
echo "### Check we signal two more rogues packets"
nb=$(docker-compose exec -T  collector \
   sh -c 'curl -s localhost:9700/metrics | grep "^canary_packet_received.*Ipv4.*$"' |
   cut -f2 -d' ')

test "$nb" -ge 4

echo
echo "### Check in logs that we signal packets"
docker-compose logs collector | grep 'Ipv4.*ICMP echo request.*seq=0'
docker-compose logs collector | grep 'Ipv4.*ICMP echo request.*seq=1'
if [[ ${CI_COMMIT_REF_NAME:-} == "" ]] # for some reason arp cache isn't empty on CI
then
   docker-compose logs collector | grep 'ARP.*ArpOperation(2)'
fi

# Test for issue #2
echo
echo "### Check that packet not addressed to canary are not logged (issue #2)"
docker-compose exec -T rogue ./send_eth_not_canary.py
docker-compose exec -T  collector \
   sh -c 'curl -s localhost:9700/metrics | ( ! grep  "02:42:ac:11:55:07" )'

echo "SUCCESS"
