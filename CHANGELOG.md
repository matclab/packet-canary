# Next release

# v0.0.4
- correct bug #2 where packet not send to canary where logged

# v0.0.3
- better CI
- some end to end tests on a docker-compose
- building a canary-collector docker on CI


# v0.0.1 : Initial release
All basic functionalities:
- listening for packets (excluding some ARP packets)
- sending alert to canary-collector
- sending alive packets to canary-collector
- buiding prometheus metrics

